import pygame
from pygame.locals import *
from . import event, load
import json

class Game():
	def __init__(self):
		pygame.init()
		self.surface = pygame.display.set_mode()
		self.screen = None
		info = pygame.display.Info()
		self.size = (info.current_w, info.current_h)
		self.init_game()

	def load_screen(self, filename):
		self.screen = load.get_object(json.load(open("screens/testscreen.json")), None, self)

	def init_game(self):
		pass

	def internal_loop(self):
		for event in pygame.event.get(): self.process_event(event)
		if self.screen:
			self.screen.draw_all(self.surface)
		self.main_loop()
		pygame.display.flip()

	def main_loop(self):
		pass

	def process_event(self, e):
		if e.type == pygame.QUIT:
			self.exit_game()
		if e.type == pygame.KEYDOWN:
			self.handle_keydown(event.KeyDownEvent(e))
		if e.type == pygame.KEYUP:
			self.handle_keyup(event.KeyUpEvent(e))
		if e.type == pygame.MOUSEMOTION:
			for obj in self.screen.family():
				if obj.active and (obj.on_hover or obj.on_hover_off):
					obj.update_hover(obj.get_rect().collidepoint(e.pos))
			self.handle_mousemove(event.MouseMoveEvent(e))
		if e.type == pygame.MOUSEBUTTONDOWN:
			for obj in self.screen.family():
				if obj.active and obj.on_click:
					if obj.get_rect().collidepoint(e.pos):
						obj.on_click(obj)
			self.handle_mouseup(event.MouseDownEvent(e))
		if e.type == pygame.MOUSEBUTTONUP:
			self.handle_mousedown(event.MouseUpEvent(e))
		else:
			self.handle_pygame_event(e)

	def handle_keydown(self, event):
		pass

	def handle_keyup(self, event):
		pass

	def handle_mousemove(self, event):
		pass

	def handle_mousedown(self, event):
		pass

	def handle_mouseup(self, event):
		pass

	def handle_pygame_event(self, event):
		pass

	def exit_game(self):
		pass