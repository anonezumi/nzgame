from .objects import screen, textbox, rect, object

object_ids = {
	"screen": screen.Screen,
	"textbox": textbox.TextBox,
	"rect": rect.Rect,
	"object": object.Object
}

def get_object(data, parent, g):
	return object_ids[data["type"]](data, parent, g, get_object)