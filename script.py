import importlib

scripts = {}

def script(name):
	def deco_script(func):
		scripts[name] = func
		return func
	return deco_script