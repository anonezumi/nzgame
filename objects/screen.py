from .object import Object

class Screen(Object):
	def draw(self, surface):
		surface.fill(self.color)