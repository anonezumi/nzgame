import pygame
from ..script import scripts

class Object():
	def __init__(self, data, parent, g, get_object):
		if "position" in data.keys(): self.pos_rel = data["position"]
		else: self.pos_rel = [0, 0]

		if "size" in data.keys(): self.size_rel = data["size"]
		else: self.size_rel = [1, 1]

		if "position_horizontal" in data.keys(): self.pos_horizontal = data["position_horizontal"]
		else: self.pos_horizontal = "center"

		if "position_vertical" in data.keys(): self.pos_vertical = data["position_vertical"]
		else: self.pos_vertical = "center"

		if "color" in data.keys(): self.color = data["color"]
		else: self.color = None

		if "active" in data.keys(): self.active = data["active"]
		elif parent: self.active = parent.active
		else: self.active = True

		if "on_hover" in data.keys(): self.on_hover = scripts[data["on_hover"]]
		else: self.on_hover = None

		if "on_hover_off" in data.keys(): self.on_hover_off = scripts[data["on_hover_off"]]
		else: self.on_hover_off = None

		if "on_click" in data.keys(): self.on_click = scripts[data["on_click"]]
		else: self.on_click = None

		self.hover = False
		self.parent = parent
		self.g = g

		if "children" in data.keys(): self.children = [get_object(obj, self, g) for obj in data["children"]]
		else: self.children = []

		if "on_init" in data.keys(): scripts[data["on_init"]](self)

	def get_color(self):
		if self.color: return self.color
		else: return self.parent.get_color()

	def get_pos(self):
		if not self.parent: return self.pos_rel
		ppos = self.parent.get_pos()
		psize = self.parent.get_size()
		rel = list(self.pos_rel)
		size = self.get_size()

		if self.pos_horizontal == "center":
			rel[0] += size[0] / 2
		if self.pos_horizontal == "right":
			rel[0] += size[0]
		return [(rel[0] * psize[0]) + ppos[0], (rel[1] * psize[1]) + ppos[1]]

	def get_rect(self):
		return pygame.Rect(self.get_pos(), self.get_size())

	def get_size(self):
		if not self.parent: return self.size_rel
		psize = self.parent.get_size()
		return [self.size_rel[0] * psize[0], self.size_rel[1] * psize[1]]

	def draw_all(self, surface):
		self.draw(surface)
		for child in self.children:
			child.draw_all(surface)

	def draw(self, surface):
		pass

	def update_hover(self, hover):
		if self.hover == hover: return
		self.hover = hover
		if hover: self.on_hover(self)
		else: self.on_hover_off(self)

	def family(self):
		f = [self]
		for child in self.children:
			f += child.family()
		return f