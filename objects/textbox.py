import pygame
from .object import Object

class TextBox(Object):
	def __init__(self, data, parent, g, get_object):
		if "antialias" in data.keys(): self.antialias = data["antialias"]
		else: self.antialias = True

		self.text = data["text"]
		self.font = pygame.font.Font(data["font"], data["font_size"])

		super().__init__(data, parent, g, get_object)

	def draw(self, surface):
		print("text")
		surface.blit(self.font.render(self.text, self.antialias, self.get_color()), self.get_pos())