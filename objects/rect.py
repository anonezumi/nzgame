import pygame
from .object import Object

class Rect(Object):
	def __init__(self, data, parent, g, get_object):
		if "border_width" in data.keys(): self.border_width = data["border_width"]
		else: self.border_width = 1

		if "border_color" in data.keys(): self.border_color = data["border_color"]
		else: self.border_color = (0, 0, 0)

		super().__init__(data, parent, g, get_object)

	def draw(self, surface):
		print("rect")
		surface.fill(self.color, self.get_rect())
		pygame.draw.rect(surface, self.border_color, self.get_rect(), width=self.border_width)

	def update_hover(self, hover):
		if self.hover == hover: return
		self.hover = hover
		if hover: self.on_hover(self)
		else: self.on_hover_off(self)