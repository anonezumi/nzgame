class KeyDownEvent():
	def __init__(self, event):
		self.key = event.key
		self.mod = event.mod

class KeyUpEvent():
	def __init__(self, event):
		self.key = event.key
		self.mod = event.mod

class MouseMoveEvent():
	def __init__(self, event):
		self.pos = event.pos
		self.rel = event.rel
		self.buttons = event.buttons

class MouseDownEvent():
	def __init__(self, event):
		self.pos = event.pos
		self.button = event.button

class MouseUpEvent():
	def __init__(self, event):
		self.pos = event.pos
		self.button = event.button